<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #main {
            width: 500px;
            border: 2px solid #4d7aa2;
            border-radius: 5px;
            padding: 30px 80px;
            display: block;
            margin: auto;
        }

        #main .wrapper {

            width: 100%;

            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .wrapper .time {
            background-color: #ddd;
            padding: 5px 0;
            width: 100%;
        }

        .time {
            margin: 10px;
        }

        form {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 15px;
        }

        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 15px;

        }

        .form-group .form-label {

            width: 50%;
            background-color: #5b9bd5;
            padding: 12px;
            border: 1px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 15px;
            color: #fff;


        }

        .form-group input {
            width: 50%;
            /* line-height: 32px; */
            font-size: 24px;
            padding: 8px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        form .form-button {
            background-color: #5b9bd5;
            padding: 12px 32px;
            font-size: 15px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
            color: #fff;
            max-width: 200px;
            text-align: center;

            margin-top: 32px;
        }
    </style>
</head>

<body>
    <div id="main">

        <div class="wrapper">
            <div class="time">
                <p>Bây giờ là <?php
                                date_default_timezone_set('Asia/Ho_Chi_Minh');
                                echo date("G:i") ?> <?php echo date("d/m/Y") ?>
                </p>
            </div>
            <form action="" method="post">
                <?php echo '<div class="form-group">
                    <label class="form-label">Tên đăng nhập</label>
                    <input type="text" name="username" id="">
                    
                </div>' ?>
                <?php echo '<div class="form-group">
                    <label class="form-label">Mật khẩu</label>
                    <input type="password" name="password" id="">
                    
                </div>' ?>

                <button class="form-button">Đăng nhập</button>
            </form>
        </div>
    </div>
</body>

</html>
<!--   -->